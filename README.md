# addie's dotfiles

I wanted to make this repo for some time now, but I was too lazy to organize and cherry-pick everything from my previous installation. So when my previous installation of vanilla-Arch broke I saved everything to an external drive and after the installation of the magnificent [EndeavourOS](https://endeavouros.com/) I took the time to cherry-pick which packages I wanted to install / needed from my previous packages.txt file and which config files I rely on the most.

As you can see I have my zshrc, all of my aliases, themes, icons, and configs here. If you'd like, you're welcome to use my configs - most of them are modified vanilla stuff or from YouTubers like Luke Smith, Chris Titus or DT.

## TODO
* Read the [XDG base directories article](https://wiki.archlinux.org/index.php/XDG_Base_Directory) on the ArchWiki and find out if any of the files/folders in the root folder can be moved
* YEET
* And also `ls -a | wc -l` regularly
